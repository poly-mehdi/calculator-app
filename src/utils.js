export const buttons = [
  '7',
  '8',
  '9',
  'DEL',
  '4',
  '5',
  '6',
  '+',
  '1',
  '2',
  '3',
  '-',
  '.',
  '0',
  '/',
  'x',
]

export const isOperator = (value) => {
  return ['+', '-', 'x', '/'].includes(value)
}

export const operate = (a, b, operator, precision = 8) => {
  let result
  switch (operator) {
    case '+':
      result = a + b
      break
    case '-':
      result = a - b
      break
    case 'x':
      result = a * b
      break
    case '/':
      result = a / b
      break
    default:
      result = NaN
  }

  if (Number.isInteger(result)) {
    return result.toString()
  }

  return result.toFixed(precision)
}

export const MAX_DIGITS = 15 // Max digits allowed on input
