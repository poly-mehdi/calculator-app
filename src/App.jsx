import React, { useState, useEffect } from 'react'
import Header from './components/Header'
import Output from './components/Output'
import Buttons from './components/Buttons'
import { isOperator, operate, MAX_DIGITS } from './utils'

const getLocalStorage = () => {
  let theme = localStorage.getItem('theme')
  return theme ? JSON.parse(theme) : 'first-theme'
}

const setLocalStorage = (theme) => {
  localStorage.setItem('theme', JSON.stringify(theme))
}
const defaultTheme = getLocalStorage()

const App = () => {
  const [theme, setTheme] = useState(defaultTheme)
  const [result, setResult] = useState('0')
  const [storedNumber, setStoredNumber] = useState(0.0)
  const [operator, setOperator] = useState(null)
  const [postOperator, setPostOperator] = useState(false)

  const handleOperator = (operator) => {
    setOperator(operator)
    setStoredNumber(parseFloat(result))
  }

  const handleNumberClick = (number) => {
    if (isOperator(number)) {
      if (!postOperator) {
        setStoredNumber(
          operate(parseFloat(storedNumber), parseFloat(result), operator)
        )
        handleOperator(number)
        setPostOperator(true)
      } else {
        if (number === '-') {
          setResult('-')
          setPostOperator(false)
        } else {
          setOperator(number)
        }
      }
    } else {
      if (result === '0' || postOperator) {
        setResult(number)
        setPostOperator(false)
      } else if (result.length < MAX_DIGITS) {
        setResult(result + number)
      }
    }
  }

  const handleDelete = () => {
    setResult((prevResult) => {
      const updatedResult = prevResult.slice(0, -1)
      return updatedResult.length === 0 ? '0' : updatedResult
    })
  }

  const handleReset = () => {
    setResult('0')
    setStoredNumber(0)
    setOperator(null)
  }

  const handleResult = () => {
    let currentResult = parseFloat(result)
    if (operator) {
      currentResult = operate(
        parseFloat(storedNumber),
        parseFloat(result),
        operator
      )
    }

    setStoredNumber(currentResult)
    setResult(String(currentResult))
    setOperator(null)
  }

  const changeTheme = () => {
    if (theme === 'first-theme') {
      setTheme('second-theme')
      setLocalStorage('second-theme')
    } else if (theme === 'second-theme') {
      setTheme('third-theme')
      setLocalStorage('third-theme')
    } else if (theme === 'third-theme') {
      setTheme('first-theme')
      setLocalStorage('first-theme')
    }
  }

  useEffect(() => {
    document.documentElement.className = theme
  }, [theme])

  return (
    <main>
      <section className="calculator-container">
        <Header theme={theme} changeTheme={changeTheme} />
        <Output result={result} operator={operator} theme={theme} />
        <Buttons
          handleNumberClick={handleNumberClick}
          handleDelete={handleDelete}
          handleReset={handleReset}
          handleResult={handleResult}
        />
      </section>
    </main>
  )
}

export default App
