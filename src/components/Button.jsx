const Button = ({ button, handleNumberClick, handleDelete }) => {
  if (button === 'DEL') {
    return (
      <div className="button delete" onClick={handleDelete}>
        {button}
      </div>
    )
  } else {
    return (
      <div
        className="button"
        onClick={() => {
          handleNumberClick(button)
        }}
      >
        {button}
      </div>
    )
  }
}
export default Button
