const Output = ({ result, operator, theme }) => {
  return (
    <div className="output">
      <p style={theme === 'first-theme' ? { color: 'hsl(0, 0%, 100%)' } : {}}>
        {operator}
      </p>
      <p style={theme === 'first-theme' ? { color: 'hsl(0, 0%, 100%)' } : {}}>
        {result}
      </p>
    </div>
  )
}
export default Output
