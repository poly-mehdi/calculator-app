import { buttons } from '../utils'
import Button from './Button'
const Buttons = ({
  handleNumberClick,
  handleDelete,
  handleResult,
  handleReset,
}) => {
  return (
    <div className="calculator">
      <div className="buttons">
        {buttons.map((button, index) => {
          return (
            <Button
              key={index}
              button={button}
              handleNumberClick={handleNumberClick}
              handleDelete={handleDelete}
            />
          )
        })}
      </div>
      <div className="buttons-function">
        <div className="button reset" onClick={handleReset}>
          RESET
        </div>
        <div className="button equal" onClick={handleResult}>
          =
        </div>
      </div>
    </div>
  )
}
export default Buttons
