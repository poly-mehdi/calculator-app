const Header = ({ theme, changeTheme }) => {
  return (
    <header>
      <h1 style={theme === 'first-theme' ? { color: 'hsl(0, 0%, 100%)' } : {}}>
        calc
      </h1>
      <div className="theme-switcher-container">
        <h2
          style={theme === 'first-theme' ? { color: 'hsl(0, 0%, 100%)' } : {}}
        >
          THEME
        </h2>
        <div className="theme-switcher" onClick={changeTheme}>
          <div
            className={
              theme === 'second-theme'
                ? 'active-2 switch'
                : theme === 'third-theme'
                ? 'active-3 switch'
                : 'switch'
            }
          ></div>
        </div>
      </div>
    </header>
  )
}
export default Header
